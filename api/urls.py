from django.urls import re_path
from rest_framework import permissions
from api import views




app_name = 'api'

urlpatterns = [
    re_path(r'^usuarios/$', views.UsuarioList.as_view(), name='usuario-list'),
    re_path(r'^usuarios/<int:pk>/$', views.UsuarioDetail.as_view(), name='usuario-detail'),
    re_path(r'^entrenamientos/$', views.EntrenamientoList.as_view(), name='entrenamiento-list'),
    re_path(r'^entrenamientos/<int:pk>/$', views.EntrenamientoDetail.as_view(), name='entrenamiento-detail'),
    re_path(r'^ejercicios/$', views.EjercicioList.as_view(), name='ejercicio-list'),
    re_path(r'^ejercicios/<int:pk>/$', views.EjercicioDetail.as_view(), name='ejercicio-detail'),
    re_path(r'^suplementaciones/$', views.SuplementacionList.as_view(), name='suplementacion-list'),
    re_path(r'^suplementaciones/<int:pk>/$', views.SuplementacionDetail.as_view(), name='suplementacion-detail'),
]
