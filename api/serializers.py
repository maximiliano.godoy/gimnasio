from rest_framework.serializers import ModelSerializer
from .models import *
from entrenamiento.settings import TASK_UPLOAD_FILE_TYPES,TASK_UPLOAD_FILE_MAX_SIZE
from rest_framework import serializers
import magic

class UsuarioSerializers(ModelSerializer):
    
    class Meta:
        model=Usuario
        fields = '__all__'
 
class EntrenamientoSerializers(ModelSerializer):
    class Meta:
        model=Entrenamiento
        fields = '__all__'
 
class EjercicioSerializers(ModelSerializer):
    class Meta:
        model=Ejercicio
        fields = '__all__'


    @staticmethod
    def validate_foto(value):
        if value is None:
            return value
        magic_file = magic.Magic(mime=True)
        content_type = magic_file.from_buffer(value.read())
        if content_type not in TASK_UPLOAD_FILE_TYPES:
            raise serializers.ValidationError("Tipo de archivo no soportado. Solo se aceptan jpeg, jpg y png")
        if value.size > TASK_UPLOAD_FILE_MAX_SIZE:
            raise serializers.ValidationError(f"El tamaño debe ser menor a {TASK_UPLOAD_FILE_MAX_SIZE} bytes. El tamaño actual es {value.size} bytes")
        return value    

class SuplementacionSerializers(ModelSerializer):
    class Meta:
        model=Suplementacion
        fields = '__all__'
 
# class Entrenamiento_EjercicioSerializers(ModelSerializer):
#     class Meta:
#         model=Entrenamiento_Ejercicio
#         fields = '__all__'
 