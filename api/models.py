from django.db import models



# Create your models here.

class Usuario(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birthday = models.DateField()
    gender = models.CharField(max_length=1, choices=(('M', 'Masculino'), ('F', 'Femenino')))
    height = models.FloatField()
    weight = models.FloatField()

class Entrenamiento(models.Model):
    date = models.DateField()
    training_type = models.CharField(max_length=50)
    duration = models.PositiveIntegerField()
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, default=1)
   

class Ejercicio(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    exercise_type = models.CharField(max_length=50)
    entrenamiento = models.ManyToManyField(Entrenamiento,blank=True)
    foto = models.FileField(blank=True,null=True, upload_to="archivos/foto")
    

class Suplementacion(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    type = models.CharField(max_length=50)
    entrenamiento = models.ForeignKey(Entrenamiento, on_delete=models.CASCADE)

