from rest_framework import generics, permissions
from .models import *
from .serializers import *


class UsuarioList(generics.ListCreateAPIView):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializers
   

class UsuarioDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializers
    
  

class EntrenamientoList(generics.ListCreateAPIView):
    queryset = Entrenamiento.objects.all()
    serializer_class = EntrenamientoSerializers
    


class EntrenamientoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Entrenamiento.objects.all()
    serializer_class = EntrenamientoSerializers
    


class EjercicioList(generics.ListCreateAPIView):
    queryset = Ejercicio.objects.all()
    serializer_class = EjercicioSerializers
    


class EjercicioDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Ejercicio.objects.all()
    serializer_class = EjercicioSerializers
    


class SuplementacionList(generics.ListCreateAPIView):
    queryset = Suplementacion.objects.all()
    serializer_class = SuplementacionSerializers
    


class SuplementacionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Suplementacion.objects.all()
    serializer_class = SuplementacionSerializers
    


# class Entrenamiento_EjercicioViewSet(ModelViewSet):
#     serializer_class=Entrenamiento_EjercicioSerializers
#     queryset=Entrenamiento_Ejercicio.objects.all()
#     permission_classes = [permissions.IsAuthenticated]

